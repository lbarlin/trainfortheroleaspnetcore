﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TrainForTheRoleAspNetCore.Domain;
using TrainForTheRoleAspNetCore.Domain.Models;
using TrainForTheRoleAspNetCore.Mvc.ViewModels.Tickets;

namespace TrainForTheRoleAspNetCore.Mvc.Controllers
{
    [Authorize]
    public class TicketsController : BaseController
    {
        public TicketsController(ILogger<BaseController> logger, ServiceWrapper serviceWrapper, IConfiguration configuration) : base(logger, serviceWrapper, configuration)
        {
        }

        public async Task<IActionResult> Index(string q, int? p = 1)
        {
            return View(new IndexViewModel()
            {
                Paged = await this.ApiClient.SearchTicketsAsync(p, q)
            });
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateViewModel model)
        { 
            if (!ModelState.IsValid)
                return View(model);

            var status = (Api.TicketStatus)model.Status;

            await this.ApiClient.PostTicketAsync(new Api.Ticket()
            {
                Title = model.Title,
                Description = model.Description,
                Status = status,
                UserId = this.UserId
            });

            return RedirectToActionPermanent("Index");
        }

        public async Task<IActionResult> Update(Guid id)
        {
            var ticket = await this.ApiClient.GetTicketAsync(id);

            return View(new UpdateViewModel()
            {
                Id = ticket.Id,
                Title = ticket.Title,
                Description = ticket.Description,
                Status = (int)ticket.Status
            });
        }

        [HttpPost]
        public async Task<IActionResult> Update(Guid id, UpdateViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var status = (Api.TicketStatus)model.Status;

            await this.ApiClient.UpdateTicketAsync(new Api.Ticket()
            {
                Title = model.Title,
                Description = model.Description,
                Status = status,
                UserId = this.UserId
            });

            return RedirectToActionPermanent("Index");
        }

        public async Task<IActionResult> Delete(Guid? id)
        {
            if (!ModelState.IsValid)
                return RedirectToActionPermanent("Index");

            await this.ApiClient.DeleteTicketAsync(id.Value);

            return RedirectToActionPermanent("Index");
        }
    }
}

