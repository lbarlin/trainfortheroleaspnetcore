﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TrainForTheRoleAspNetCore.Domain;
using TrainForTheRoleAspNetCore.Mvc.Models;

namespace TrainForTheRoleAspNetCore.Mvc.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(ILogger<BaseController> logger, ServiceWrapper serviceWrapper, IConfiguration configuration) : base(logger, serviceWrapper, configuration)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
