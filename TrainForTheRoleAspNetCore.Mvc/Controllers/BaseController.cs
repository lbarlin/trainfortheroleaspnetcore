﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Api;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TrainForTheRoleAspNetCore.Domain;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TrainForTheRoleAspNetCore.Mvc.Controllers
{
    public abstract class BaseController : Controller
    {
        protected ServiceWrapper ServiceWrapper;
        protected readonly ILogger<BaseController> Logger;
        protected swaggerClient ApiClient;
        protected IConfiguration Configuration;

        protected BaseController(ILogger<BaseController> logger, ServiceWrapper serviceWrapper, IConfiguration configuration)
        {
            Logger = logger;
            ServiceWrapper = serviceWrapper;
            Configuration = configuration;

            var httpClient = new HttpClient();

            ApiClient = new swaggerClient("https://trainfortheroleaspnetcore-api.azurewebsites.net", httpClient);
        }

        protected string UserId => User.Claims.FirstOrDefault(a => a.Type == ClaimTypes.NameIdentifier).Value;
    }
}

