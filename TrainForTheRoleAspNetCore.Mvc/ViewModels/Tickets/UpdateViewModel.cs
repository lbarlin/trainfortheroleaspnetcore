﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TrainForTheRoleAspNetCore.Mvc.ViewModels.Tickets
{
    public class UpdateViewModel
    {
        [Required]
        public Guid? Id { get; set; }

        [Required, MaxLength(50)]
        public string Title { get; set; }

        [Required, MaxLength(1000)]
        public string Description { get; set; }

        [Required]
        public int? Status { get; set; }
    }
}
