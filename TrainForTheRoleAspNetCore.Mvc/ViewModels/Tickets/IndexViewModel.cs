﻿using System;
using Api;

namespace TrainForTheRoleAspNetCore.Mvc.ViewModels.Tickets
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
        }

        public TicketPagedResult Paged { get; set; }
    }
}
