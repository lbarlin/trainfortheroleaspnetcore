﻿using System;
using System.Linq;

namespace TrainForTheRoleAspNetCore.Domain.Infrastructure
{
    public static class LinqExtensions
    {
        public static PagedResult<T> GetPaged<T>(this IQueryable<T> query, int pageSize, int? page = 1)
            where T : class
        {
            return query.GetPaged("", pageSize, page);
        }

        public static PagedResult<T> GetPaged<T>(this IQueryable<T> query, string keyword, int pageSize, int? page)
            where T : class
        {
            page ??= 1;

            var result = new PagedResult<T>
            {
                CurrentPage = page.Value,

                PageSize = pageSize,

                RowCount = query.Count(),

                Keyword = keyword
            };

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page.Value - 1) * pageSize;
            result.Results = query.Skip(skip).Take(pageSize).ToList();

            return result;
        }
    }
}

