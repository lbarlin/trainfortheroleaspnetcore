﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using TrainForTheRoleAspNetCore.Domain.Enums;

namespace TrainForTheRoleAspNetCore.Domain.Models
{
    public class Ticket : BaseModel
    {
        public Ticket()
        {
            this.Status = TicketStatus.Pending;
        }

        [Required, MaxLength(450)]
        public string UserId { get; set; }
        public IdentityUser User { get; set; }

        [Required, MaxLength(50)]
        public string Title { get; set; }

        [Required, MaxLength(1000)]
        public string Description { get; set; }

        public TicketStatus Status { get; set; }
    }
}

