﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TrainForTheRoleAspNetCore.Domain.Models
{
    public abstract class BaseModel
    {
        protected BaseModel()
        {
            this.Timestamp = DateTime.UtcNow;
        }

        [Key]
        public Guid? Id { get; set; }

        [Required]
        public DateTime? Timestamp { get; set; }
    }
}

