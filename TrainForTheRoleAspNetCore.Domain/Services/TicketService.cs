﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TrainForTheRoleAspNetCore.Domain.Enums;
using TrainForTheRoleAspNetCore.Domain.Infrastructure;
using TrainForTheRoleAspNetCore.Domain.Models;

namespace TrainForTheRoleAspNetCore.Domain.Services
{
    public class TicketService : BaseService<Ticket>
    {
        public TicketService(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public Ticket Get(Guid id)
        {
            return this.All().Include(a => a.User)
                .FirstOrDefault(a => a.Id == id);
        }

        public PagedResult<Ticket> Get(int? page, int pageSize = 10, string keyword = "")
        {
            var query = this.All().Include(a => a.User).AsQueryable();

            if(!string.IsNullOrWhiteSpace(keyword))
            {
                query = query.Where(a => a.Title.Contains(keyword) ||
                 a.Description.Contains(keyword));
            }

            query = query.OrderByDescending(a => a.Timestamp);

            return query.GetPaged(keyword, pageSize, page);
        }

        public async Task UpdateStatus(Guid ticketId, TicketStatus status)
        {
            var ticket = this.DbContext.Set<Ticket>()
                .FirstOrDefault(a => a.Id == ticketId);

            if (ticket == null)
                return;

            ticket.Status = status;

            this.DbContext.Set<Ticket>()
                .Update(ticket);

            await this.SaveChangesAsync();
        }

        public async Task Update(Ticket model)
        {
            var ticket = this.DbContext.Set<Ticket>()
                .FirstOrDefault(a => a.Id == model.Id);

            if (ticket == null)
                return;

            ticket.Title = model.Title;
            ticket.Description = model.Description;
            ticket.Status = model.Status;
            
            this.DbContext.Set<Ticket>()
                .Update(ticket);

            await this.SaveChangesAsync();
        }

        public async Task Delete(Guid ticketId)
        {
            var ticket = this.DbContext.Set<Ticket>()
                .FirstOrDefault(a => a.Id == ticketId);

            if (ticket == null)
                return;

            this.DbContext.Set<Ticket>()
                .Remove(ticket);

            await this.SaveChangesAsync();
        }

    }
}

