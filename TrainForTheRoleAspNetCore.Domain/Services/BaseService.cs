﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TrainForTheRoleAspNetCore.Domain.Services
{
    public abstract class BaseService<T> where T : class
    {
        protected ApplicationDbContext DbContext { get; set; }

        protected BaseService(ApplicationDbContext dbContext)
        {
            this.DbContext = dbContext;
        }
        public IQueryable<T> All()
        {
            return this.DbContext.Set<T>().AsNoTracking();
        }
        public IQueryable<T> All(Expression<Func<T, bool>> expression)
        {
            return this.DbContext.Set<T>()
                .Where(expression).AsNoTracking();
        }

        public void Create(T entity)
        {
            this.DbContext.Set<T>().Add(entity);
        }

        //protected void Update(T entity)
        //{
        //    this.DbContext.Set<T>().Update(entity);
        //}

        //protected void Delete(T entity)
        //{
        //    this.DbContext.Set<T>().Remove(entity);
        //}

        public async Task SaveChangesAsync()
        {
            await this.DbContext.SaveChangesAsync();
        }
    }
}

