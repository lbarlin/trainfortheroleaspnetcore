﻿using System;
using TrainForTheRoleAspNetCore.Domain.Services;

namespace TrainForTheRoleAspNetCore.Domain
{
    public class ServiceWrapper
    {
        private readonly ApplicationDbContext _dbContext;

        public ServiceWrapper(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        private TicketService _tickets;
        public TicketService Tickets => _tickets ??= new TicketService(_dbContext);
    }
}

