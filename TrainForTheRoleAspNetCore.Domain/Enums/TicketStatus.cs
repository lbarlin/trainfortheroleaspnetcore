﻿using System;
namespace TrainForTheRoleAspNetCore.Domain.Enums
{
    public enum TicketStatus
    {
        Pending,
        InProgree,
        Closed
    }
}

