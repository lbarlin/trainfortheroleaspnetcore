using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using TrainForTheRoleAspNetCore.Domain;
using TrainForTheRoleAspNetCore.Domain.Infrastructure;
using TrainForTheRoleAspNetCore.Domain.Models;

namespace TrainForTheRoleAspNetCore.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketsController : BaseController
    {
        public TicketsController(ILogger<TicketsController> logger, ServiceWrapper serviceWrapper)
            : base(logger, serviceWrapper)
        {
        }

        [HttpGet(Name = "SearchTickets"), SwaggerOperation(Summary = "Search for Tickets", Description = "Search for Tickets Description")]
        public PagedResult<Ticket> Get(int? page, string keyword = "")
        {
            var retVal = this.ServiceWrapper.Tickets.Get(page, 10, keyword);

            return retVal;
        }

        [HttpGet("{id}", Name = "GetTicket"), SwaggerOperation(Summary = "Get Ticket", Description = "Description......")]
        public Ticket Get(Guid id)
        {
            return this.ServiceWrapper.Tickets.Get(id);
        }

        [HttpPost(Name = "PostTicket")]
        public async Task Post([FromBody] Ticket value)
        {
            if (!ModelState.IsValid)
                return;

            this.ServiceWrapper.Tickets.Create(value);
            await this.ServiceWrapper.Tickets.SaveChangesAsync();
        }

        [HttpPut(Name = "UpdateTicket")]
        public async Task Put([FromBody] Ticket value)
        {
            if (!ModelState.IsValid)
                return;

            await this.ServiceWrapper.Tickets.Update(value);
        }

        [HttpDelete("{id}", Name = "DeleteTicket")]
        public async Task Delete([Required] Guid? id)
        {
            if (!ModelState.IsValid)
                return;

            await this.ServiceWrapper.Tickets.Delete(id.Value);
        }
    }
}
