﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TrainForTheRoleAspNetCore.Domain;

namespace TrainForTheRoleAspNetCore.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public abstract class BaseController : ControllerBase
    {
        protected ServiceWrapper ServiceWrapper;
        protected readonly ILogger<BaseController> Logger;

        protected BaseController(ILogger<BaseController> logger, ServiceWrapper serviceWrapper)
        {
            Logger = logger;
            ServiceWrapper = serviceWrapper;
        }
    }
}

